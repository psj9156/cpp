#include <iostream>

inline int SQUARE(int x) // 인라인 함수 : 일반 함수에 비해 실행속도(성능향상)의 이점이 있음
{
	return x * x;
}

int main(void)
{
	std::cout << SQUARE(5) << std::endl;
	std::cout << SQUARE(12) << std::endl;
	return 0;
}