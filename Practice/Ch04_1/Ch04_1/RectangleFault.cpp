#include <iostream>
using namespace std;

class Point
{
public:
	int x; // x좌표의 범위 : 0 ~ 100
	int y; // y좌표의 범위 : 0 ~ 100
};

class Rectangle
{
public:
	Point upLeft;
	Point lowRight;

	void ShowRecInfo()
	{
		cout << "좌 상단: " << '[' << upLeft.x << ", "
		<< upLeft.y << ']' << endl;

		cout << "우 하단: " << '[' << lowRight.x << ", "
			<< lowRight.y << ']' << endl;
	}
};

int main(void)
{
	Point pos1 = { -2, 4 }; // 멤버변수가 public으로 선언되면 구조체 변수를 초기화하듯이 초기화 가능
	Point pos2 = { 5, 9 };

	Rectangle rec = { pos2, pos1 }; // 멤버 변수를 통해 초기화하듯이 Point의 객체를 전달하여 rec 객체를 생성 및 초기화.
	rec.ShowRecInfo();

	return 0;
}

// 문제점 1 : 점의 좌표는 0~100 이어야 하는데, 그렇지 못한 객체가 있다.
// 문제점 2 : 직사각형을 의미하는 Rectangle 객체의 좌상단 좌표값이 우하단 좌표값보다 크다.