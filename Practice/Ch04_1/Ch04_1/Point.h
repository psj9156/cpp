#pragma once

class Point
{ // 멤버변수 x, y를 private으로 선언하여 정보 은닉
private:
	int x;
	int y;

public:
	bool InitMembers(int xpos, int ypos); // 값의 저장 및 참조를 위한 함수 정의
	int GetX() const;
	int GetY() const;
	bool SetX(int xpos);
	bool SetY(int ypos);
};