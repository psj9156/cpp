#include <iostream>
#include "Point.h"
using namespace std;

bool Point::InitMembers(int xpos, int ypos)
{
	if (xpos < 0 || ypos < 0)
	{
		cout << "벗어난 범위의 값 전달" << endl;
		return false;
	}
	else
	{
		x = xpos;
		y = ypos;
		return true;
	}
}

int Point::GetX() const // const 함수 : 이 함수 내에서는 멤버변수에 저장된 값을 변경하지 않겠다! (매개변수, 지역변수 x)
{ // const 함수 내에서는 const가 아닌 함수의 호출이 제한됨 (const가 아닌 함수에서는 멤버변수의 값을 변경할 '가능성'이 있기 때문)
	return x;
}

int Point::GetY() const
{
	return y;
}

bool Point::SetX(int xpos)
{
	if (xpos < 0 || xpos > 100)
	{
		cout << "벗어난 범위의 값 전달" << endl;
		return false;
	}
	else
	{
		x = xpos;
		return true;
	}
}

bool Point::SetY(int ypos)
{
	if (ypos < 0 || ypos > 100)
	{
		cout << "벗어난 범위의 값 전달" << endl;
		return false;
	}
	else
	{
		y = ypos;
		return true;
	}
}

// Get...(), Set...() 함수 : 엑세스 함수(access function) - 멤버변수를 private으로 선언하면서,
// 클래스 외부에서의 멤버변수 접근을 목적으로 정의되는 함수 (정의되었으나 호출되지 않는 경우도 있음)