#include <iostream>
using namespace std;

class SoSimple
{
private:
    int num;
public:
    SoSimple(int n) : num(n)
    { }
    SoSimple& AddNum(int n)
    {
        num += n;
        return *this;
    }
    void ShowData() const // const 멤버함수
    {
        cout << "num: " << num << endl;
    }
};

int main() {
    const SoSimple obj(7); // const 객체 생성
    // obj.AddNum(20); // const 객체는 const 함수 이외에는 호출 불가능!
    obj.ShowData();
    return 0;
}
