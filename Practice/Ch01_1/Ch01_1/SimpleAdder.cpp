#include <iostream>

int main(void)
{
	int val1;
	std::cout << "첫 번째 숫자 입력: ";
	std::cin >> val1; // 키보드로부터 정수 하나를 입력 받아서 변수 val1에 저장하라

	int val2;
	std::cout << "두 번째 숫자 입력: ";
	std::cin >> val2;

	int result = val1 + val2;

	std::cout << "덧셈결과 : " << result << std::endl;
	return 0;
}