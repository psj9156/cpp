#include <iostream> // 입출력을 위한 헤더파일 선언 (표준 헤더파일 선언은 "*.h" 대신 "<*>")

int main(void)
{
	int num = 20;
	// std::cout << '출력대상'; - 서식문자(%d, %s 등)를 이용하여 별도의 출력포맷을 지정하지 않아도
	// 데이터의 성격에 따라 적절한 출력이 이루어짐!

	// << 라는 연산자를 이용하여 둘 이상의 출력대상을 연이어서 출력 가능
	std::cout << "Hello World!" << std::endl;// std::endl - 개행
	std::cout << "Hello " << "World!" << std::endl;
	std::cout << num << ' ' << 'A';
	std::cout << ' ' << 3.14 << std::endl;
	return 0;
}