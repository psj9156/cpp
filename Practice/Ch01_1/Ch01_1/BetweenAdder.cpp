#include <iostream>

// 두 개의 정수를 입력 받아서 그 사이에 존재하는 정수들의 합을 계산하여 출력하는 프로그램
int main(void)
{
	int val1, val2;
	int result = 0;
	std::cout << "두 개의 숫자 입력 : ";
	std::cin >> val1 >> val2; // 연속적으로 데이터 입력 가능 (데이터 간의 경계 : 탭, 스페이스 바, Enter)

	if (val1 < val2)
	{
		for (int i = val1 + 1; i < val2; i++) // for문 내에서 변수 선언 가능
		{
			result += i;
		}
	}
	else
	{
		for (int i = val2 + 1; i < val1; i++)
		{
			result += i;
		}
	}

	std::cout << "두 수 사이의 정수 합 : " << result << std::endl;
	
	return 0;
}