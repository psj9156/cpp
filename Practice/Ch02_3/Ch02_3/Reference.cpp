#include <iostream>
using namespace std;

int main(void)
{
	int num1 = 1020;
	int& num2 = num1;

	num2 = 3047;
	cout << "VAL: " << num1 << endl;
	cout << "REF: " << num2 << endl; // 동일한 값 출력 - num1과 num2가 같은 메모리 공간을 참조함

	cout << "VAL: " << &num1 << endl;
	cout << "REF: " << &num2 << endl; // 동일한 주소 값 출력
	return 0;
}