#include <iostream>

using namespace std;

class First
{
public:
    virtual void MyFunc() { cout << "FirstFunc" << endl; }
};

class Second : public First
{
public:
    virtual void MyFunc() { cout << "SecondFunc" << endl; }
};

class Third : public Second
{
public:
    virtual void MyFunc() { cout << "ThirdFunc" << endl; }
};

int main() {
    Third *tptr = new Third(); // ThirdFunc
    Second *sptr = tptr; // ThirdFunc
    First *fptr = new Second(); // SecondFunc

    // 원래는 포인터의 자료형을 기반으로 호출대상을 결정하지만
    // 함수가 가상함수로 호출되면 포인터 변수가 실제로 가리키는 객체를 참조하여 호출 대상을 결정한다.
    fptr->MyFunc();
    sptr->MyFunc();
    tptr->MyFunc();
    delete tptr;
    return 0;
}
