#include <iostream>
#include <cstring>

using namespace std;

namespace RISK_LEVEL
{
    enum
    {
        RISK_A = 30, RISK_B = 20, RISK_C = 10
    };
}

class Employee // 기초 클래스로서만 의미를 가지고, 객체 생성을 하지 않음. - 가상함수를 순수 가상함수로 선
{
private:
    char name[100];
public:
    Employee(char *name)
    {
        strcpy(this->name, name);
    }
    void ShowYourName() const
    {
        cout << "name: " << name << endl;
    }
    virtual int GetPay() const = 0; // 순수 가상함수 : 함수 몸체가 정의되지 않은 함수
    virtual void ShowSalaryInfo() const = 0; // 순수 가상함수 - Employee 객체(잘못된 객체)를 생성할 경우 컴파일 에
};

class PermanentWorker : public Employee
{
private:
    int salary; // 월급
public:
    PermanentWorker(char* name, int money)
            : Employee(name), salary(money)
    { }
    int GetPay() const
    {
        return salary;
    }
    void ShowSalaryInfo() const
    {
        ShowYourName();
        cout << "salary: " << GetPay() << endl << endl;
    }
};

class TemporaryWorker : public Employee
{
private:
    int workTime; // 이 달에 일한 시간
    int payPerHour; // 시간당 급여
public:
    TemporaryWorker(char *name, int pay)
            : Employee(name), workTime(0), payPerHour(pay)
    { }
    void AddWorkTime(int time) // 일한 시간의 추가
    {
        workTime += time;
    }
    int GetPay() const // 이 달의 급여
    {
        return workTime * payPerHour;
    }
    void ShowSalaryInfo() const
    {
        ShowYourName();
        cout << "salary: " << GetPay() << endl << endl;
    }
};

class SalesWorker : public PermanentWorker // PermanentWorker를 상속한다는 것 유의!
{
private:
    int salesResult; // 월 판매 실적
    double bonusRatio; // 상여금 비율
public:
    SalesWorker(char *name, int money, double ratio)
            : PermanentWorker(name, money), salesResult(0), bonusRatio(ratio)
    { }
    void AddSalesResult(int value)
    {
        salesResult += value;
    }
    int GetPay() const
    {
        return PermanentWorker::GetPay() // PermanentWorker의 GetPay 함수 호출
               + (int)(salesResult * bonusRatio);
    }
    void ShowSalaryInfo() const
    {
        ShowYourName();
        cout << "salary: " << GetPay() << endl << endl; // SalesWorker의 GetPay 함수 호
    }
};

class ForeignSalesWorker : public SalesWorker
{
private:
    const int risk;
public:
    ForeignSalesWorker(char *name, int money, double ratio, int riskLevel)
    : SalesWorker(name, money, ratio), risk(riskLevel)
    { }
    int GetPay() const
    {
        return (int)(SalesWorker::GetPay() * (1 + risk/100.0));
    }
    void ShowSalaryInfo() const
    {
        ShowYourName();
        cout << "salary: " << GetPay() << endl << endl;
    }
};

class EmployeeHandler
{
private:
    Employee *empList[50];
    int empNum;
public:
    EmployeeHandler() : empNum(0) { }
    void AddEmployee(Employee *emp)
    {
        empList[empNum++] = emp;
    }
    void ShowAllSalaryInfo() const
    {
        for(int i = 0; i < empNum; i++)
            empList[i]->ShowSalaryInfo();
    }
    void ShowTotalSalary() const
    {
        int sum = 0;
        for (int i = 0; i < empNum; i++)
            sum += empList[i]->GetPay();

        cout << "salary sum: " << sum << endl;
    }
    ~EmployeeHandler()
    {
        for (int i = 0; i < empNum; i++)
            delete empList[i];
    }
};

int main() {
    EmployeeHandler handler;

    handler.AddEmployee(new PermanentWorker("KIM", 1000));
    handler.AddEmployee(new PermanentWorker("LEE", 1500));

    TemporaryWorker *alba = new TemporaryWorker("Jung", 700);
    alba->AddWorkTime(5);
    handler.AddEmployee(alba);

    SalesWorker *seller = new SalesWorker("Hong", 1000, 0.1);
    seller->AddSalesResult(7000);
    handler.AddEmployee(seller);

    // 해외 영업직 등록
    ForeignSalesWorker *fseller1 = new ForeignSalesWorker("James", 1000, 0.1, RISK_LEVEL::RISK_A);
    fseller1->AddSalesResult(7000);
    handler.AddEmployee(fseller1);

    ForeignSalesWorker *fseller2 = new ForeignSalesWorker("Park", 1000, 0.1, RISK_LEVEL::RISK_B);
    fseller2->AddSalesResult(7000);
    handler.AddEmployee(fseller2);

    ForeignSalesWorker *fseller3 = new ForeignSalesWorker("Charles", 1000, 0.1, RISK_LEVEL::RISK_C);
    fseller3->AddSalesResult(7000);
    handler.AddEmployee(fseller3);

    handler.ShowAllSalaryInfo();

    handler.ShowTotalSalary();
    return 0;
}
