#pragma warning(disable:4996)
#include <iostream>
#include <cstring>
using namespace std;

class Person
{
private:
	char* name;
	int age;
public:
	Person(const char* myname, int myage)
	{
		int len = strlen(myname) + 1;
		name = new char[len]; // 생성자에서 new에 의해 동적 할당
		strcpy(name, myname);
		age = myage;
	}
	// 깊은복사를 위해 추가
	Person(const Person& copy) : age(copy.age)
	{
		name = new char[strlen(copy.name) + 1];
		strcpy(name, copy.name);
	}
	void ShowPersonInfo() const
	{
		cout << "이름: " << name << endl;
		cout << "나이: " << age << endl;
	}
	~Person()
	{
		delete[] name; // 소멸자에서 delete에 의해 메모리 해제
		cout << "Called Destructor!" << endl;
	}
};

int main(void)
{
	Person man1("Lee dong woo", 29);
	Person man2 = man1; // 복사 생성자를 따로 추가하지 않으면 
	// 디폴트 복사 생성자에 의한 멤버 대 멤버의 얕은 복사 진행

	man1.ShowPersonInfo();
	man2.ShowPersonInfo();

	// 얕은 복사만 한 경우, man2에서 delete가 일어날 때 이미 문자열이 사라진 뒤이다.
	// - 실행이 되지만 오류 발생!

	return 0;
}