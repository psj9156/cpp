#include <iostream>

using namespace std; // 이름공간 std에 선언된 모든 것에 대해 이름공간 지정을 생략함

int main(void)
{
	int num = 20;
	cout << "Hello World!" << endl;
	cout << "Hello " << "World!" << endl;
	cout << num << ' ' << 'A';
	cout << ' ' << 3.14 << endl;
	return 0;
}