#include <iostream>

using std::cin;
using std::cout;
using std::endl; // 전역변수처럼 프로그램 실행 이후 종료 시 까지 효력이 있음.

int main(void)
{
	int num = 20;
	cout << "Hello World!" << endl;
	cout << "Hello " << "World!" << endl;
	cout << num << ' ' << 'A';
	cout << ' ' << 3.14 << endl;
	return 0;
}