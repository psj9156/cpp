#include <iostream>
using namespace std;

class SoSimple
{
private:
	int num1;
	int num2;
public:
	SoSimple(int n1, int n2)
		: num1(n1), num2(n2)
	{

	}
	SoSimple(const SoSimple& copy) // 복사 생성자 (copy constructor)
		: num1(copy.num1), num2(copy.num2)
	{ // 멤버 대 멤버의 복사에 사용되는 원본을 변경시키는 것은 실수할 가능성이 있으니 const 삽입.
		cout << "Called SoSimple(SoSimple &copy)" << endl;
	}
	void ShowSimpleData()
	{
		cout << num1 << endl;
		cout << num2 << endl;
	}
};

int main(void)
{
	SoSimple sim1(15, 30);
	cout << "생성 및 초기화 직전" << endl;

	SoSimple sim2 = sim1; // SoSimple sim2(sim1); 으로 묵시적으로 변환됨
	cout << "생성 및 초기화 직후" << endl;

	sim2.ShowSimpleData();

	return 0;
}