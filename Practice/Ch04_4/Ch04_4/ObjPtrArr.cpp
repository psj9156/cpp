#pragma warning(disable:4996)
#include <iostream>
#include <cstring>
using namespace std;

class Person
{
private:
	char* name;
	int age;
public:
	Person(char* myname, int myage)
	{
		int len = strlen(myname) + 1;
		name = new char[len];
		strcpy(name, myname);
		age = myage;
	}
	Person() // 객체 배열 생성 시 void형 생성자가 호출됨.
	{
		name = NULL; // NULL pointer
		age = 0;
		cout << "Called Person()" << endl;
	}
	/*void SetPersonInfo(char* myname, int myage) // 필요없어짐
	{
		name = myname;
		age = myage;
	}*/
	void ShowPersonInfo() const
	{
		cout << "이름: " << name << ", ";
		cout << "나이: " << age << endl;
	}
	~Person()
	{
		delete[] name;

		cout << "Called destructor!" << endl;
	}
};

int main(void)
{
	Person* parr[3]; // 이번에는 객체 포인터 배열 생성 (객체의 주소 값 3개를 저장하는 배열)
	char namestr[100];
	int age;

	for (int i = 0; i < 3; i++)
	{
		cout << "이름: ";
		cin >> namestr;
		cout << "나이: ";
		cin >> age;
		parr[i] = new Person(namestr, age); // 객체를 생성하여 객체의 주소 값을 배열에 저장
	}

	parr[0]->ShowPersonInfo();
	parr[1]->ShowPersonInfo();
	parr[2]->ShowPersonInfo();

	delete parr[0];
	delete parr[1];
	delete parr[2]; // new 연산을 3번 했으므로 delete연산도 3번!

	return 0;
}

// ObjArr : 저장의 대상을 객체로 설정
// ObjPtrArr : 저장의 대상을 객체의 주소값으로 설정