#include <iostream>
using namespace std;

class TwoNumber
{
private:
	int num1;
	int num2;

public:
	TwoNumber(int num1, int num2)
	{
		this->num1 = num1; // this->num1 : 멤버변수, num1 : 매개변수
		this->num2 = num2;
	}
	// 멤버이니셜랴이저에서는 this 포인터 사용 불가능하지만 괄호 밖은 멤버변수,
	// 괄호 안은 매개변수로 인식을 하기 때문에 다음과 같이 대신할 수 있다.
	/*TwoNumber(int num1, int num2)
		: num1(num1), num2(num2)
	{
		...
	}*/
	void ShowTwoNumber()
	{
		cout << this->num1 << endl; // 멤버변수에 접근함을 명확히 하기 위해 this를 사용했지만
		cout << this->num2 << endl; // 일반적으로는 this 생략하여 표현.
	}
};

int main(void)
{
	TwoNumber two(2, 4);
	two.ShowTwoNumber();
	return 0;
}