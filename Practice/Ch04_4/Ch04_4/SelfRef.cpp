#include <iostream>
using namespace std;

class SelfRef
{
private:
	int num;
public:
	SelfRef(int n) : num(n)
	{
		cout << "객체 생성" << endl;
	}
	SelfRef& Adder(int n)
	{ // 반환형이 참조형이므로 객체 자신을 참조할 수 있는 참조 정보(참조값) 반환
		num += n;
		return *this; // 객체 자신을 반환
	}
	SelfRef& ShowTwoNumber()
	{
		cout << num << endl;
		return *this;
	}
};

int main(void)
{
	SelfRef obj(3);
	SelfRef& ref = obj.Adder(2); // 참조자 ref는 객체 obj를 참조함.

	obj.ShowTwoNumber(); // 5
	ref.ShowTwoNumber(); // 5

	ref.Adder(1).ShowTwoNumber().Adder(2).ShowTwoNumber(); // 6, 8

	obj.ShowTwoNumber(); // 8

	return 0;
}