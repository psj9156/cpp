#include <iostream>
#include <cstring>
using namespace std;

class Girl; // Girl이라는 이름이 클래스의 이름임을 알림 (함수 원형과 비슷)

class Boy
{
private:
    int height;
    friend class Girl; // Girl 클래스에 대해 friend 선언
    // (Girl 클래스 내에서는 Boy클래스의 모든 private 멤버에 직접 접근 가능)
    // 하지만 이는 캡슐화와 정보은닉을 망침! - 필요한 경우가 아니면 쓰지 않기!
    // 5행이 없어도 컴파일 됨 - 위 선언이 Gril이라는 것이 클래스 이름임을 알려주기 때문!
public:
    Boy(int len) : height(len)
    { }
    void ShowYourFriendInfo(Girl &frn); // 원형 선언을 5행에서 해주었기 때문에 컴파일 가
};

class Girl
{
private:
    char phNum[20];
public:
    Girl(char* num)
    {
        strcpy(phNum, num);
    }
    void ShowYourFriendInfo(Boy &frn);
    friend class Boy; // Boy 클래스에 대한 friend 선언
};

void Boy::ShowYourFriendInfo(Girl &frn) {
    cout << "Her phone number: " << frn.phNum << endl;
    // 컴파일러가 위 행을 컴파일하기 위해서는 Gril클래스에 phNum이라는 멤버변수의 존재를 알아야 함
    // 그래서 Girl 클래스가 이 함수보다 앞에 온 것.
}

void Girl::ShowYourFriendInfo(Boy &frn) {
    cout << "His height: " << frn.height << endl;
}

int main() {
    Boy boy(170);
    Girl girl("010-1234-5678");

    boy.ShowYourFriendInfo(girl);
    girl.ShowYourFriendInfo(boy);

    return 0;
}
