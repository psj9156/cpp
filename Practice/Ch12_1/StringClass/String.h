//
// Created by 박상준 on 2020/09/14.
//

#ifndef STRINGCLASS_STRING_H
#define STRINGCLASS_STRING_H

#include <iostream>
#include <cstring>

using namespace std;

class String {
private:
    int len;
    char *str;
public:
    String();
    String(const char *s);
    String(const String &s);
    ~String();
    String &operator=(const String &s);
    String &operator+=(const String &s);
    bool operator==(const String &s);
    String operator+(const String &s);

    friend ostream &operator<<(ostream &os, const String &s);
    friend istream &operator>>(istream &is, String &s);
};

#endif //STRINGCLASS_STRING_H
