#pragma warning(disable:4996)
#include <iostream>
#include <string.h>
using namespace std;

char* MakeStrAdr(int len)
{
	// char* str = (char*)malloc(sizeof(char)*len); // malloc, free (in C)
	char* str = new char[len]; // new, delete (in C++)
	return str;
}

int main(void)
{
	char* str = MakeStrAdr(20);
	strcpy(str, "I am so happy~");
	cout << str << endl;
	// free(str)
	delete[]str;
	return 0;
}

//															New							Delete
// int형 변수 할당 및 소멸						int* ptr1 = new int;				delete ptr1;
// double형 변수 할당 및 소멸					double* ptr2 = new double;			delete ptr2;
// 길이가 3인 int형 배열의 할당 및 소멸			int* arr1 = new int[3];				delete[]arr1;
// 길이가 7인 double형 배열의 할당 및 소멸		double* arr2 = new double[7];		delete[]arr2;