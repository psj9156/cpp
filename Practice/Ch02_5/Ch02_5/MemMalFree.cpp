// 길이 정보를 인자로 받아서, 해당 길이의 문자열 저장이 가능한 배열을 생성하고, 그 배열의 주소 값을 반환하는 함수
#pragma warning(disable:4996)
#include <iostream>
#include <string.h>
#include <stdlib.h> // C++에서 C언어의 헤더파일 추가하는 것도 가능.
using namespace std;

// C언어에서의 동적 할당

char* MakeStrAdr(int len)
{
	char* str = (char*)malloc(sizeof(char) * len); // malloc : 문자열 저장을 위한 배열을 힙 영역에 할당
	return str;
}

int main(void)
{
	char* str = MakeStrAdr(20);
	strcpy(str, "I am so happy~");
	cout << str << endl;
	free(str); // free : 힙에 할당된 메모리 공간 소멸시킴
	return 0;
}