#include <iostream>
#include <stdlib.h>
using namespace std;

class Simple
{
public:
	Simple()
	{
		cout << "I'm simple constructor!" << endl;
	}
};

int main(void)
{
	cout << "case 1 : ";
	Simple* sp1 = new Simple;

	cout << "case 2 : ";
	Simple* sp2 = (Simple*)malloc(sizeof(Simple) * 1);

	// malloc&free와 new&delete의 동작방식이 다르다는 정도만 알아두기

	cout << endl << "end of main" << endl;
	delete sp1;
	free(sp2);
	return 0;
}