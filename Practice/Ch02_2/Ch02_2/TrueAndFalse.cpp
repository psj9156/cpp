#include <iostream>
using namespace std;

int main(void)
{
	int num = 10;
	int i = 0;

	cout << "true : " << true << endl;
	cout << "false : " << false << endl; // 키워드 true, false를 콘솔에 출력했을 때의 출력 내용

	while (true) // 무한루프에서 1 대신 true 사용 가능
	{
		cout << i++ << ' ';
		if (i > num) break;
	}

	cout << endl;
	cout << "sizeof 1: " << sizeof(1) << endl;
	cout << "sizeof 0: " << sizeof(0) << endl;
	cout << "sizeof true: " << sizeof(true) << endl;
	cout << "sizeof false: " << sizeof(false) << endl; // 데이터 true와 false의 크기
	return 0;
}