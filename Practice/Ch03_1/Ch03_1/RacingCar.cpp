#include <iostream>
using namespace std;

// 구조체 Car과 관련된 정보를 상수화함 - 구조체 내에서 enum으로 대체 가능!
/*
#define ID_LEN 20
#define MAX_SPD 200
#define	FUEL_STEP 2
#define ACC_STEP 10
#define BRK_STEP 10 
*/

// 가독성을 높이고, 특정 구조체에서만 사용될 수도 있기 때문에 별도의 이름공간을 활용하여 열거형 (enum) 사용

namespace CAR_CONST
{
	enum
	{
		ID_LEN = 20,
		MAX_SPD = 200,
		FUEL_STEP = 2,
		ACC_STEP = 10,
		BRK_STEP = 10
	};
}

struct Car
{
	char gamerID[CAR_CONST::ID_LEN]; // 소유자 ID
	int fuelGauge; // 연료량
	int curSpeed; // 현재속도

	void ShowCarState(/*const Car& car*/) // 차의 정보를 출력하는 함수
	{
		{
			cout << "소유자ID: " << gamerID << endl; // 구조체 내에 함수를 포함시킬 경우 'car.변수' 를 할 필요가 없다.
			cout << "연료량: " << fuelGauge << "%" << endl;
			cout << "현재속도: " << curSpeed << "km/s" << endl << endl;
		}
	}

	void Accel(/*Car& car*/) // 차의 가속을 위해 엑셀을 밟은 상황을 표현한 함수
	{
		if (fuelGauge <= 0) return;
		else fuelGauge -= CAR_CONST::FUEL_STEP;

		if (curSpeed + CAR_CONST::ACC_STEP >= CAR_CONST::MAX_SPD)
		{
			curSpeed = CAR_CONST::MAX_SPD;
			return;
		}

		curSpeed += CAR_CONST::ACC_STEP;
	}

	void Break(/*Car& car*/) // 브레이크를 밟은 상황을 표현한 함수
	{
		if (curSpeed < CAR_CONST::BRK_STEP)
		{
			curSpeed = 0;
			return;
		}

		curSpeed -= CAR_CONST::BRK_STEP;
	}
};

int main(void)
{
	Car run99 = { "run99", 100, 0 }; // 구조체 변수의 선언 및 초기화 (함수를 구조체 내부에 집어넣더라도 방법은 같음)

	/* 함수들이 구조체 밖에 있는 경우
	Accel(run99);
	Accel(run99);
	ShowCarState(run99);
	Break(run99);
	ShowCarState(run99);
	*/

	// 함수를 구조체 내로 집어넣은 경우
	run99.Accel(); // 구조체 run99에 존재하는(이해를 편하게 하기 위해 각 구조체에 함수와 변수가 포함되는 것으로 이해)
	// Accel 함수를 호출. 구조체 내에 선언된 변수에 접근하는 방법과 똑같이 함수를 호출함.
	run99.Accel();
	run99.ShowCarState();
	run99.Break();
	run99.ShowCarState();

	Car sped77 = { "sped77", 100, 0 };
	/*함수들이 구조체 밖에 있는 경우
	Accel(sped77);
	Break(sped77);
	ShowCarState(sped77);
	*/

	// 함수를 구조체 내로 집어넣은 경우
	sped77.Accel();
	sped77.Break();
	sped77.ShowCarState();
	
	return 0;
}