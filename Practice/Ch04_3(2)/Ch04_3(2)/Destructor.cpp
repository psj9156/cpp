#pragma warning(disable:4996)
#include <iostream>
#include <cstring>
using namespace std;

class Person
{
private:
	char* name;
	int age;

public:
	Person(const char myname[], int myage)
	{
		int len = strlen(myname) + 1; // +1 : NULL문자 감안
		name = new char[len]; // 문자열의 길이만큼 동적 할당
		strcpy(name, myname); // myname을 name으로 복사
		age = myage;
	}
	void ShowPersonInfo() const
	{
		cout << "이름: " << name << endl;
		cout << "나이: " << age << endl;
	}
	~Person() // 소멸자 : 생성자 형식 (반환형 x) 앞에 ~붙음, 매개변수 void형 - 오버로딩, 디폴트값설정 불가
	{
		delete[] name;
		cout << "called destructor!" << endl;
	}
};

int main(void)
{
	Person man1("Lee dong woo", 29);
	Person man2("Jang dong gun", 41);
	man1.ShowPersonInfo();
	man2.ShowPersonInfo();
	return 0;
}