#include <iostream>
#include "Rectangle_AddConstructor.h"
using namespace std;

Rectangle::Rectangle(int x1, int y1, const int& x2, const int& y2)
	:upLeft(x1, y1), lowRight(x2, y2) // 객체 upLeft 생성과정에서 x1, y1을 인자로 전달받는 생성자 호출
	// 객체 lowRight 생성 과정에서 x2, y2를 인자로 전달받는 생성자 호출
{
	// empty
}

void Rectangle::ShowRecInfo() const
{
	cout << "좌 상단: " << '[' << upLeft.GetX() << ", "
		<< upLeft.GetY() << ']' << endl;
	cout << "우 하단: " << '[' << lowRight.GetX() << ", "
		<< lowRight.GetY() << ']' << endl << endl;
}