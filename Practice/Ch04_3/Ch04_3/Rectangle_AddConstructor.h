#pragma once
#include "Point_AddConstructor.h"

class Rectangle
{
private:
	Point upLeft;
	Point lowRight;
public:
	Rectangle(int x1, int y1, const int& x2, const int& y2);
	void ShowRecInfo() const;
};