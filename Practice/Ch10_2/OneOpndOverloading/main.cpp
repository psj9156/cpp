#include <iostream>

using namespace std;

class Point
{
private:
    int xpos, ypos;
public:
    Point (int x = 0, int y = 0) : xpos(x), ypos(y)
    { }
    void ShowPosition() const
    {
        cout << '[' << xpos << ", " << ypos << ']' << endl;
    }
    Point& operator++() // 멤버함수 형태로 오버로딩
    {
        xpos += 1;
        ypos += 1;
        return *this;
    }
    friend Point& operator--(Point &ref);
};

Point& operator--(Point &ref) // 전역함수 형태로 오버로딩
{
    ref.xpos -= 1;
    ref.ypos -= 1;
    return ref; // 객체 자신의 참조값 반환 - 연속으로 사용 가능!
}

int main()
{
    Point pos(1, 2);
    ++pos; // pos.operator++();
    pos.ShowPosition();
    --pos; // operator--(pos);
    pos.ShowPosition();

    ++(++pos);
    pos.ShowPosition();
    --(--pos);
    pos.ShowPosition();
    return 0;
}