#include <iostream>
using namespace std;

class SinivelCap // 콧물 처치용 캡슐
{
public:
	void Take() const { cout << "콧물 처치!" << endl; }
};

class SneezeCap // 재채기 처치용 캡슐
{
public:
	void Take() const { cout << "재채기 처치!" << endl; }
};

class SnuffleCap // 코막힘 처치용 캡슐
{
public:
	void Take() const { cout << "코 뚫림!" << endl; }
};

class CONTAC600 // 캡슐화 된 코감기약 CONTAC600 클래스 정의
{
private:
	SinivelCap sin;
	SneezeCap sne;
	SnuffleCap snu;

public:
	void Take() const
	{
		sin.Take();
		sne.Take();
		snu.Take();
	}
};

class ColdPatient // 클래스가 매우 간결해짐
{
public:
	void TakeCONTAC600(const CONTAC600& cap) const { cap.Take(); }
};

int main(void) // 약 복용 과정도 간결해짐
{
	CONTAC600 cap;
	ColdPatient sufferer;

	sufferer.TakeCONTAC600(cap); 
	return 0;
}

// 약 복용순서가 바뀌어도 ColdPatient 클래스 입장에서는 고민할 필요가 없다. CONTAC600 클래스 내부에서
// 멤버함수의 변경만 해주면 된다.