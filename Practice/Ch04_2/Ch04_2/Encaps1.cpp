#include <iostream>
using namespace std;

class SinivelCap // 콧물 처치용 캡슐
{
public:
	void Take() const { cout << "콧물 처치!" << endl; }
};

class SneezeCap // 재채기 처치용 캡슐
{
public:
	void Take() const { cout << "재채기 처치!" << endl; }
};

class SnuffleCap // 코막힘 처치용 캡슐
{
public:
	void Take() const { cout << "코 뚫림!" << endl; }
};

class ColdPatient
{
public:
	void TakeSinivelCap(const SinivelCap& cap) const { cap.Take(); }
	void TakeSneezeCap(const SneezeCap& cap) const { cap.Take(); }
	void TakeSnuffleCap(const SnuffleCap& cap) const { cap.Take(); }
};

int main(void)
{
	SinivelCap scap;
	SneezeCap zcap;
	SnuffleCap ncap;

	ColdPatient sufferer;
	sufferer.TakeSinivelCap(scap);
	sufferer.TakeSneezeCap(zcap);
	sufferer.TakeSnuffleCap(ncap);

	return 0;
}

// 만약 "코감기는 항상 콧물, 재채기, 코막힘을 동반한다" 라고 가정하면, 캡슐화가 무너진 코드이다.
// 코감기에 걸렸을 때, 복용 과정을 3번이나 거쳐야 하는데, 하나의 클래스로 만들어 놓았다면(캡슐화 했다면)
// 복용 과정이 훨씬 간소화된다.
// 더 큰 문제는, 복용 순서가 정해져있다면 SinivelCap, SneezeCap, SnuffleCap 클래스 간의 상호관계까지
// 따로 생각해야하는 상황에 놓인다. 따라서 객체의 활용 또한 매우 어려워진다.