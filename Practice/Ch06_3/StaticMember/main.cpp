// static 멤버변수 (클래스 변수) : 객체가 생성되든 말든 메모리 공간에 딱 하나만 할당되어 공유됨.
// 전역변수보다 안정적 (생성 및 소멸 시점은 전역변수와 동일)
#include <iostream>
using namespace std;

class SoSimple
{
private:
    static int simObjCnt; // SoSimple 객체 내에서 공
public:
    SoSimple()
    {
        simObjCnt++; // 멤버 함수 내에서 멤버변수와 접근방법은 같지만 성질은 다름!
        cout << simObjCnt << "번째 SoSimple 객체" << endl;
    }
};
int SoSimple::simObjCnt = 0; // static 변수의 초기화

class SoComplex
{
private:
    static int cmxObjCnt;
public:
    SoComplex()
    {
        cmxObjCnt++;
        cout << cmxObjCnt<< "번째 SoComplex 객체" << endl;
    }
    SoComplex(SoComplex& copy)
    {
        cmxObjCnt++;
        cout << cmxObjCnt << "번째 SoComplex 객체" << endl;
    }
};
int SoComplex::cmxObjCnt = 0;

int main() {
    SoSimple sim1;
    SoSimple sim2;

    SoComplex cmx1;
    SoComplex cmx2 = cmx1;
    SoComplex();

    return 0;
}
