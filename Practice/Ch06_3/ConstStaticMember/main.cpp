// static 멤버함수 역시 static 멤버변수와 특성이 동일함
// 1. 선언된 클래스의 모든 객체가 공유됨
// 2. public으로 선언되면, 클래스 이름을 이용하여 호출 가능
// 3. 객체의 멤버로 존재하지 않음 - 주의!
// : static 멤버함수 내에서는 static 멤버변수, 다른 static 멤버함수만 호출 가능

// const static으로 선언되는 멤버변수(상수)는 선언과 동시에 초기화 가능!
#include <iostream>
using namespace std;

class CountryArea
{
public:
    const static int RUSSIA     = 1707540;
    const static int CANADA     = 998467;
    const static int CHINA      = 957290;
    const static int SOUTH_KOREA = 9922;
};

int main(void)
{
    cout << "러시아 면적: " << CountryArea::RUSSIA << "km^2" << endl;
    cout << "캐나다 면적: " << CountryArea::CANADA << "km^2" << endl;
    cout << "중국 면적: " << CountryArea::CHINA << "km^2" << endl;
    cout << "한국 면적: " << CountryArea::SOUTH_KOREA << "km^2" << endl;

    return 0;
}