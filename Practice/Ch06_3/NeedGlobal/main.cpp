// 전역변수가 필요한 상황 연출
#include <iostream>
using namespace std;

int simObjCnt = 0;
int cmxObjCnt = 0;
// 전역변수는 제한을 지켜줄만한 아무런 장치도 없다. (어디서든 접근 가능)

class SoSimple
{
public:
    SoSimple()
    {
        simObjCnt++;
        cout << simObjCnt << "번째 SoSimple 객체" << endl;
    }
};

class SoComplex
{
public:
    SoComplex()
    {
        cmxObjCnt++;
        cout << cmxObjCnt << "번째 SoComplex 객체" << endl;
    }
    SoComplex(SoComplex& copy)
    {
        cmxObjCnt++;
        cout << cmxObjCnt << "번째 SoComplex rorcp" << endl;
    }
};

int main() {
    SoSimple sim1;
    SoSimple sim2;

    SoComplex com1;
    SoComplex com2 = com1;
    SoComplex();
    return 0;
}
