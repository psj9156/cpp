// C++에서 C언어의 표준함수 사용하기 : 헤더파일의 확장자인 .h를 생략하고 앞에 c를 붙이면 C언어에 대응하는
// C++의 헤더파일 이름이 된다.
#pragma warning(disable:4996)

#include <cmath>
#include <cstdio>
#include <cstring>
using namespace std; // 이름공간 std 내에 선언되어 있다는 사실을 제외하면, C++의 헤더는 C언어의 헤더와 별 차이가 없다.

int main(void)
{
	char str1[] = "Result";
	char str2[30];

	strcpy(str2, str1);
	printf("%s : %f \n", str1, sin(0.14));
	printf("%s : %f \n", str2, abs(-1.25));
	return 0;
}