#include <iostream>
using namespace std;

class SoSimple
{
private:
	int num;
public:
	SoSimple(int n) : num(n) {}
	SoSimple(const SoSimple& copy) : num(copy.num)
	{
		cout << "Called SoSimple(const SoSimple& copy)" << endl;
	}
	SoSimple& AddNum(int n)
	{
		num += n;
		return *this;
	}
	void ShowData()
	{
		cout << "num: " << num << endl;
	}
};

SoSimple SimpleFuncObj(SoSimple ob)
{
	cout << "return 이전" << endl;
	return ob; // 이 때, 반환형이 참조가 아니므로, 임시 객체에 할당한다.
}

int main(void)
{
	SoSimple obj(7);
	SimpleFuncObj(obj)/*이 때 임시객체 생성*/.AddNum(30).ShowData();
	obj.ShowData(); // 임시 객체만 AddNum 되었으므로 이건 그대로 7!
	return 0;
}