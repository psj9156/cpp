#pragma once // 헤더파일의 중복포함을 막기위해 사용 (#ifndef ~ #endif도 있음)
// C++에서의 파일 분할
// header file (Car.h) : 클래스의 선언을 담는다. - 클래스를 구성하는 외형적인 틀
// source file (Car.cpp) : 클래스(멤버 함수)의 정의를 담는다.
// main source file (RacingMain.cpp) : 메인함수를 담는다.

namespace CAR_CONST
{
	enum
	{
		ID_LEN = 20, MAX_SPD = 200, FUEL_STEP = 2, ACC_STEP = 10, BRK_STEP = 10
	};
}
class Car
{
private:
	char gamerID[CAR_CONST::ID_LEN];
	int fuelGauge;
	int curSpeed;
public:
	void InitMembers(const char* ID, int fuel);
	void ShowCarState();
	void Accel();
	void Break();
};
