#pragma warning(disable:4996)
#include <iostream>
#include <cstring>
using namespace std;

namespace CAR_CONST
{
	enum
	{
		ID_LEN = 20, MAX_SPD = 200, FUEL_STEP = 2, ACC_STEP = 10, BRK_STEP = 10
	};
}

class Car // 구조체와의 차이점 : 키워드, 구조체 변수(객체) 생성, 접근제어
{
private: // 클래스 내에서만 접근 허용
	char gamerID[CAR_CONST::ID_LEN];
	int fuelGauge;
	int curSpeed;
public: // 어디서든 접근 허용
	void InitMembers(const char* ID, int fuel);
	void ShowCarState();
	void Accel();
	void Break();
};
// 함수 정의를 클래스 밖에서 하더라도, 클래스의 일부이기 때문에 private으로 선언된 변수에 접근 가능
inline void Car::InitMembers(const char* ID, int fuel) // 멤버를 초기화해주는 함수
{
	strcpy(gamerID, ID);
	fuelGauge = fuel;
	curSpeed = 0; // 초기화 시 속도는 항상 0
}
inline void Car::ShowCarState()
{
	cout << "소유자ID: " << gamerID << endl;
	cout << "연료량: " << fuelGauge << "%" << endl;
	cout << "현재속도: " << curSpeed << "km/s" << endl << endl;
}
inline void Car::Accel()
{
	if (fuelGauge <= 0) return;
	else fuelGauge -= CAR_CONST::FUEL_STEP;

	if ((curSpeed + CAR_CONST::ACC_STEP) >= CAR_CONST::MAX_SPD)
	{
		curSpeed = CAR_CONST::MAX_SPD;
		return;
	}
	curSpeed += CAR_CONST::ACC_STEP;
}
inline void Car::Break()
{
	if (curSpeed < CAR_CONST::BRK_STEP)
	{
		curSpeed = 0;
		return;
	}
	curSpeed -= CAR_CONST::BRK_STEP;
}

int main(void)
{
	Car run99; // 객체 생성
	run99.InitMembers("run99", 100); // 초기화시키는 함수를 따로 정의하고 호출해야함.
	run99.Accel();
	run99.Accel();
	run99.Accel();
	run99.ShowCarState();
	run99.Break();
	run99.ShowCarState();

	return 0;
}

// 키워드 'struct'를 이용해서 정의한 구조체(클래스에 포함됨)에 선언된 변수와 함수에 별도의 접근제어
// 지시자를 선언하지 않으면, 모든 변수와 함수는 public으로 선언 (즉, Default : public)

// 키워드 'class'를 이용해서 정의한 클래스에 선언된 변수와 함수에 별도의 접근제어 지시자를 선언하지
// 않으면, 모든 변수와 함수는 private으로 선언 (즉, Default : private)