#include <iostream>
using namespace std;

int& RefRetFuncOne(int& ref) // 반환형이 참조형인 함수는 반환 값을 변수 또는 참조자로, 두 가지의 형태로 저장 가능(num2, num4)
{
	ref++;
	return ref;
}

int RefRetFuncTwo(int& ref) // 반환형이 기본자료형으로 선언된 함수는 반드시 변수에 저장되어야 함.(num3)
{
	ref++;
	return ref;
}

int main(void)
{
	int num1 = 1;
	int& num2 = RefRetFuncOne(num1); // num2는 num1의 별칭 (같은 메모리 공간을 나타냄)
	int num3 = RefRetFuncTwo(num1); // num3는 num1을 참조하여 FuncTwo함수를 적용한 결과의 값을 나타냄. (다른 메모리 공간)
	int num4 = RefRetFuncOne(num1); // num4도 num1과 완전히 다른 변수.

	num1++;
	num2++;
	num4 += 200;
	cout << "num1 : " << num1 << endl; // 6
	cout << "num2 : " << num2 << endl; // 6
	cout << "num3 : " << num3 << endl; // 3
	cout << "num4 : " << num4 << endl; // 204

	return 0;
}