#include <iostream>
#include "NameCard.h"

int main(void)
{
	NameCard manClerk("Lee", "ABCEng", "010-1111-2222", COMP_RANK::CLERK);
	NameCard manSENIOR("Hong", "OrangeEng", "010-3333-4444", COMP_RANK::SENIOR);
	NameCard manASSIST("Kim", "SoGoodComp", "010-5555-6666", COMP_RANK::ASSIST);

	manClerk.ShowNameCardInfo();
	manSENIOR.ShowNameCardInfo();
	manASSIST.ShowNameCardInfo();

	return 0;
}