#pragma once

namespace COMP_RANK
{
	enum
	{
		CLERK, SENIOR, ASSIST, MANAGER
	};
	void ShowRankInfo(int r);
}

class NameCard
{
private:
	char* name;
	char* compname;
	char* phnum;
	int rank;

public:
	NameCard(const char* myname, const char* cname, const char* phonenumber, int crank);
	void ShowNameCardInfo();
	~NameCard();
};