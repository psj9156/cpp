#pragma warning(disable:4996)
#include <iostream>
#include "NameCard.h"
#include <cstring>
using namespace std;

void COMP_RANK::ShowRankInfo(int r)
{
	switch (r)
	{
	case CLERK:
		cout << "사원" << endl;
		break;
	case SENIOR:
		cout << "주임" << endl;
		break;
	case ASSIST:
		cout << "대리" << endl;
		break;
	case MANAGER:
		cout << "과장" << endl;
	}
}

NameCard::NameCard(const char* myname, const char* cname, const char* phonenumber, int crank)
{
	int len1 = strlen(myname) + 1;
	name = new char[len1];
	strcpy(name, myname);

	int len2 = strlen(cname) + 1;
	compname = new char[len2];
	strcpy(compname, cname);

	int len3 = strlen(phonenumber) + 1;
	phnum = new char[len3];
	strcpy(phnum, phonenumber);

	rank = crank;
}

void NameCard::ShowNameCardInfo()
{
	cout << "이름 : " << name << endl;
	cout << "회사 : " << compname << endl;
	cout << "전화번호 : " << phnum << endl;
	cout << "직급 : "; COMP_RANK::ShowRankInfo(rank); 
	cout << endl << endl;
}

NameCard::~NameCard()
{ // 동적할당 한 변수들에 대한 delete
	delete[] name;
	delete[] compname;
	delete[] phnum;
}