// Question. 2차원 평면 상에서의 좌표를 표현할 수 있는 구조체를 다음과 같이 정의하였다.
// struct Point
// {
//		int xpos;
//		int ypos;
// };
// 위 구조체를 기반으로 다음의 함수를 정의하고자 한다.
// void MovePos(int x, int y); // 점의 좌표이동
// void AddPoint(const Point &pos); // 점의 좌표증가
// void ShowPosition(); // 현재 x, y 좌표정보 출력
// 단, 위의 함수들을 구조체 안에 정의하여 main 함수는 주어진 대로 구성한다.
// 실행결과 :
// [5, 14]
// [25, 44]

#include <iostream>
using namespace std;

struct Point
{
	int xpos;
	int ypos;

	void MovePos(int x, int y)
	{
		xpos += x;
		ypos += y;
	}

	void AddPoint(Point* pos)
	{
		xpos += pos->xpos;
		ypos += pos->ypos;
	}

	void ShowPosition()
	{
		cout << '[' << xpos << ", " << ypos << ']' << endl;
	}
};

int main(void)
{
	Point pos1 = { 12, 4 };
	Point pos2 = { 20, 30 };

	pos1.MovePos(-7, 10);
	pos1.ShowPosition();

	pos1.AddPoint(&pos2);
	pos1.ShowPosition();
	return 0;
}