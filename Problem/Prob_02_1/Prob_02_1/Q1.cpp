// 참조자를 이용해서 다음 요구사항에 부합하는 함수를 각각 정의하여라.
// 인자로 전달된 int형 변수의 값을 1씩 증가시키는 함수
// 인자로 전달된 int형 변수의 부호를 바꾸는 함수
// 그리고 위의 각 함수를 호출하여 그 결과를 확인하는 main함수까지 작성하여라.

// num1, num2를 각각 가리키는 ptr1, ptr2를 대상으로 SwapPointer(ptr1, ptr2)라는 함수를 호출하면 ptr1과 ptr2가
// 가리키는 대상이 서로 바뀌도록 하는 함수를 정의해보라.

#include <iostream>
using namespace std;

int AddOne(int& ref1)
{
	return ref1 + 1;
}

int ChangeSign(int& ref2)
{
	return -ref2;
}

void SwapPointer(int* (&ref3), int* (&ref4))
{
	int* temp = ref3;
	ref3 = ref4;
	ref4 = temp;
}

int main(void)
{
	int val1 = 23;
	
	cout << "val1 : " << val1 << endl;
	cout << "AddOne : " << AddOne(val1) << endl;
	cout << "ChangeSign : " << ChangeSign(val1) << endl;

	int num1 = 5;
	int* ptr1 = &num1;
	int num2 = 10;
	int* ptr2 = &num2;

	cout << "Original Pointer : " << ptr1 << ' ' << ptr2 << endl;
	SwapPointer(ptr1, ptr2);
	cout << "After Swapping : " << ptr1 << ' ' << ptr2 << endl;

	return 0;
}