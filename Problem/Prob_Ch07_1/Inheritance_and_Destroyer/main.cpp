#include <iostream>
#include <cstring>

using namespace std;

class MyFriendInfo {
private:
    char *name;
    int age;
public:
    MyFriendInfo(char *fname, int fage) : age(fage) {
        name = new char[strlen(fname) + 1];
        strcpy(name, fname);
        cout << "MyFriendInfo() " << endl;
    }

    void ShowMyFriendInfo() {
        cout << "이름: " << name << endl;
        cout << "나이: " << age << endl;
    }

    ~MyFriendInfo() {
        delete[] name;
        cout << "~MyFriendInfo() " << endl;
    }
};

class MyFriendDetailInfo : public MyFriendInfo {
private:
    char *addr;
    char *phone;
public:
    MyFriendDetailInfo(char *fname, int fage, char *faddr, char *fphone)
            : MyFriendInfo(fname, fage) {
        addr = new char[strlen(faddr) + 1];
        strcpy(addr, faddr);

        phone = new char[strlen(fphone) + 1];
        strcpy(phone, fphone);

        cout << "MyFriendDetailInfo() " << endl;
    }

    void ShowMyFriendDetailInfo() {
        ShowMyFriendInfo();
        cout << "주소: " << addr << endl;
        cout << "전화: " << phone << endl << endl;
    }

    ~MyFriendDetailInfo() {
        delete[] addr;
        delete[] phone;
        cout << "~MyFriendDetailInfo() " << endl;
    }
};

int main() {
    MyFriendDetailInfo dinfo("Park", 25, "천호대로", "010-4513-8201");
    dinfo.ShowMyFriendDetailInfo();
    return 0;
}
