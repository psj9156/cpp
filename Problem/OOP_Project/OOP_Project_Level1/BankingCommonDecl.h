//
// Created by �ڻ��� on 2020/09/06.
//

#ifndef OOP_PROJECT_LEVEL1_BANKINGCOMMONDECL_H
#define OOP_PROJECT_LEVEL1_BANKINGCOMMONDECL_H

namespace CREDIT_LEVEL
{
    enum
    {
        CREDIT_A = 7, CREDIT_B = 4, CREDIT_C = 2
    };
}

namespace WHICH_ACCOUNT
{
    enum
    {
        NORMAL = 1, CREDIT = 2
    };
}

#endif OOP_PROJECT_LEVEL1_BANKINGCOMMONDECL_H
