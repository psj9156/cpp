//
// Created by �ڻ��� on 2020/09/06.
//

#ifndef OOP_PROJECT_LEVEL1_HIGHCREDITACCOUNT_H
#define OOP_PROJECT_LEVEL1_HIGHCREDITACCOUNT_H
#include <iostream>
#include "NormalAccount.h"

class HighCreditAccount : public NormalAccount // �ſ�ŷڰ���
{
private:
    const int specialRate;
public:
    HighCreditAccount(int cusnum, char myname[], int Rate, int CreditLevel)
            : NormalAccount(cusnum, myname, Rate), specialRate(CreditLevel + Rate)
    { }
    virtual void Deposit(int money)
    {
        Account::Deposit(money * (1 + specialRate/100.0));
    }
    virtual void ShowAccInfo() const
    {
        Account::ShowAccInfo();
        std::cout << "����: " << specialRate << "%" << std::endl;
    }
};

#endif OOP_PROJECT_LEVEL1_HIGHCREDITACCOUNT_H
