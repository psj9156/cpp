//
// Created by 박상준 on 2020/09/06.
//
#include <iostream>
#include <cstring>
#include "Account.h"

using namespace std;

Account::Account(int cusnum, char myname[])
{
    accID = cusnum + 1;			// 고객 수보다 1 많도록 지정

    int len = strlen(myname) + 1;
    name = new char[len];
    strcpy(name, myname);

    balance = 0;				// 초기 자금 : 0
}

Account::Account(const Account& copy) // 아직 호출되지는 않음
        : accID(copy.accID)
{
    name = new char[strlen(copy.name)];
    strcpy(name, copy.name);
}

int Account::GetAccID() const { return accID; }

void Account::Deposit(int money)
{
    balance += money;
}

int Account::Withdraw(int money) // 출금액 반환 (부족 시 0 반환)
{
    if (balance < money) return 0;

    balance -= money;
    return money;
}

void Account::ShowAccInfo() const
{
    cout << "계좌ID: " << accID << endl;
    cout << "이 름: " << name << endl;
    cout << "잔 액: " << balance << endl;
}

Account::~Account()
{
    delete[] name;
}