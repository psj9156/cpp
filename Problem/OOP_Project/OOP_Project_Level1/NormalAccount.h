//
// Created by 박상준 on 2020/09/06.
//

#ifndef OOP_PROJECT_LEVEL1_NORMALACCOUNT_H
#define OOP_PROJECT_LEVEL1_NORMALACCOUNT_H
#include "Account.h"
#include <iostream>

using namespace std;

class NormalAccount : public Account // 보통예금계좌
{
private:
    const int interRate; // 이율 (%)
public:
    NormalAccount(int cusnum, char myname[], int rate)
            : Account(cusnum, myname), interRate(rate)
    { }
    virtual void Deposit(int money)
    {
        Account::Deposit(money * (1 + interRate/100.0)); // 원금, 이자 추가
    }
    virtual void ShowAccInfo() const
    {
        Account::ShowAccInfo();
        cout << "이율: " << interRate << "%" << endl << endl;
    }
};

#endif OOP_PROJECT_LEVEL1_NORMALACCOUNT_H
