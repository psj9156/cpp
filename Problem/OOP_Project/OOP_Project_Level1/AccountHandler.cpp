//
// Created by 박상준 on 2020/09/06.
//

#include "BankingCommonDecl.h"
#include "AccountHandler.h"
#include "Account.h"
#include "NormalAccount.h"
#include "HighCreditAccount.h"

using namespace std;

void AccountHandler::Home(void)
{
    cout << "----------Menu----------" << endl;
    cout << "1. 계좌개설 " << endl;
    cout << "2. 입 금 " << endl;
    cout << "3. 출 금 " << endl;
    cout << "4. 계좌 정보 전체 출력 " << endl;
    cout << "5. 프로그램 종료 " << endl << endl;
}

void AccountHandler::MakeAccount(void)
{
    int num;
    cout << "[계좌 종류 선택]" << endl;
    cout << "1. 보통예금계좌   2. 신용신뢰계좌 " << endl;
    cout << "선택: ";
    cin >> num;

    if(num == WHICH_ACCOUNT::NORMAL)
        MakeNormalAccount();
    else if(num == WHICH_ACCOUNT::CREDIT)
        MakeCreditAccount();
    else
        cout << "잘못된 입력입니다." << endl;
}

void AccountHandler::MakeNormalAccount(void)
{
    char name[100];
    int interRate;

    cout << "[보통예금계좌 개설]" << endl;
    cout << "계좌ID: " << numCustomer + 1 << endl;
    cout << "이 름: "; cin >> name;
    cout << "잔 액: 0" << endl;
    cout << "이자율: "; cin >> interRate;
    cout << endl;

    accArr[numCustomer++] = new NormalAccount(numCustomer, name, interRate);
}
void AccountHandler::MakeCreditAccount(void)
{
    char name[100];
    int interRate;
    int creditLevel;

    cout << "[신용신뢰계좌 개설]" << endl;
    cout << "계좌ID: " << numCustomer + 1 << endl;
    cout << "이 름: "; cin >> name;
    cout << "잔 액: 0" << endl;
    cout << "이자율: "; cin >> interRate;
    cout << "신용등급(1 - A, 2 - B, 3 - C): "; cin >> creditLevel;
    cout << endl;

    switch(creditLevel)
    {
        case 1:
            accArr[numCustomer++] = new HighCreditAccount(numCustomer, name, interRate, CREDIT_LEVEL::CREDIT_A);
            break;
        case 2:
            accArr[numCustomer++] = new HighCreditAccount(numCustomer, name, interRate, CREDIT_LEVEL::CREDIT_B);
            break;
        case 3:
            accArr[numCustomer++] = new HighCreditAccount(numCustomer, name, interRate, CREDIT_LEVEL::CREDIT_C);
            break;
        default:
            cout << "잘못된 값을 입력하셨습니다. " << endl << endl;
    }
}

void AccountHandler::DepositMoney(void)
{
    int money;
    int id;

    cout << "[입 금]" << endl;
    cout << "계좌ID: "; cin >> id;
    cout << "입금액: "; cin >> money;

    for (int i = 0; i < numCustomer; i++)
    {
        if (accArr[i]->GetAccID() == id)
        {
            accArr[i]->Deposit(money);
            cout << "입금 완료" << endl << endl;
            return;
        }
    }
    cout << "유효하지 않은 ID 입니다." << endl << endl;
}

void AccountHandler::WithdrawMoney(void)
{
    int money;
    int id;

    cout << "[출 금]" << endl;
    cout << "계좌ID: "; cin >> id;
    cout << "출금액: "; cin >> money;

    for (int i = 0; i < numCustomer; i++)
    {
        if (accArr[i]->GetAccID() == id)
        {
            if (accArr[i]->Withdraw(money) == 0)
            {
                cout << "잔액 부족" << endl << endl;
                return;
            }
            cout << "출금 완료" << endl << endl;
            return;
        }
    }
    cout << "유효하지 않은 ID 입니다." << endl << endl;
}

void AccountHandler::ShowAllAccInfo(void)
{
    for (int i = 0; i < numCustomer; i++)
    {
        accArr[i]->ShowAccInfo();
        cout << endl;
    }
}

AccountHandler::~AccountHandler()
{
    for(int i = 0; i < numCustomer; i++)
    {
        delete accArr[i];
    }
}