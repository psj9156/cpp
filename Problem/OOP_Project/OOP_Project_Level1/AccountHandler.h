//
// Created by 박상준 on 2020/09/06.
//

#ifndef OOP_PROJECT_LEVEL1_ACCOUNTHANDLER_H
#define OOP_PROJECT_LEVEL1_ACCOUNTHANDLER_H
#include "Account.h"

class AccountHandler // 컨트롤 클래스 정의
{
private:
    int numCustomer = 0;
    Account *accArr[100]; // Account 저장을 위한 배열(객체 포인터 배열)
public:
    void Home(void);
    void MakeAccount(void);
    void MakeNormalAccount(void);
    void MakeCreditAccount(void);
    void DepositMoney(void);
    void WithdrawMoney(void);
    void ShowAllAccInfo(void);

    ~AccountHandler();
};

#endif OOP_PROJECT_LEVEL1_ACCOUNTHANDLER_H
