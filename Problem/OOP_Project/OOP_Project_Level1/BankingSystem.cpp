//Banking System Ver 0.7
//작성자 : 박상준
//내용 :  파일 분할
#pragma warning(disable:4996)
#include <iostream>
#include "AccountHandler.h"

using namespace std;

int main(void)
{
    AccountHandler manager;
	int num;

	while (1)
	{
		manager.Home();
		cout << "선택: ";
		cin >> num;
		cout << endl;

		switch (num)
		{
		case 1:
			manager.MakeAccount();
			break;
		case 2:
			manager.DepositMoney();
			break;
		case 3:
			manager.WithdrawMoney();
			break;
		case 4:
			manager.ShowAllAccInfo();
			break;
		case 5:
			return 0;
		default:
			cout << "잘못된 값을 입력했습니다." << endl << endl;
		}
	}

	return 0;
}