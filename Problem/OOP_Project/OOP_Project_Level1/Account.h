//
// Created by 박상준 on 2020/09/06.
//

#ifndef OOP_PROJECT_LEVEL1_ACCOUNT_H
#define OOP_PROJECT_LEVEL1_ACCOUNT_H

class Account
{
private:
    int accID;
    char* name;
    int balance;
public:
    Account(int cusnum, char myname[]);
    Account(const Account& copy); // 아직 호출되지는 않음

    int GetAccID() const;
    virtual void Deposit(int money);
    int Withdraw(int money); // 출금액 반환 (부족 시 0 반환)
    virtual void ShowAccInfo() const;

    virtual ~Account();
};


#endif OOP_PROJECT_LEVEL1_ACCOUNT_H
