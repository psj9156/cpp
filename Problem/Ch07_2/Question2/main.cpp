#include <iostream>
#include <cstring>

using namespace std;

class Book {
private:
    char *title;        // 책의 제목
    char *isbn;         // 국제표준도서번호
    int price;          // 책의 정가
public:
    Book(char *bookTitle, char *bookNum, int bookPrice)
            : price(bookPrice)
    {
        title = new char[strlen(bookTitle) + 1];
        strcpy(title, bookTitle);

        isbn = new char[strlen(bookNum) + 1];
        strcpy(isbn, bookNum);
    }
    void ShowBookInfo()
    {
        cout << "책 제목: " << title << endl;
        cout << "국제표준도서번호: " << isbn << endl;
        cout << "책 가격: " << price << endl;
    }
    ~Book()
    {
        delete[] title;
        delete[] isbn;
    }
};

class EBook : public Book {
private:
    char *DRMKey;       // 보안관련 키
public:
    EBook(char *bookTitle, char *bookNum, int bookPrice, char *bookKey)
    : Book(bookTitle, bookNum, bookPrice)
    {
        DRMKey = new char[strlen(bookKey) + 1];
        strcpy(DRMKey, bookKey);
    }
    void ShowEBookInfo()
    {
        ShowBookInfo();
        cout << "보안 키: " << DRMKey << endl;
    }
    ~EBook()
    {
        delete[] DRMKey;
    }

};

int main() {
    Book book("좋은 C++", "555-12345-890-0", 20000);
    book.ShowBookInfo();
    cout << endl;
    EBook ebook("좋은 C++ ebook", "555-12345-890-1", 10000, "fdx9w0i8kiw");
    ebook.ShowEBookInfo();
    return 0;
}
