#include <iostream>
#include "Paint.h"
using namespace std;

void Point::Init(int x, int y)
{
	xpos = x;
	ypos = y;
}
void Point::ShowPointInfo() const
{
	cout << "Point�� ��ǥ: ";
	cout << "[" << xpos << ", " << ypos << "]" << endl;
}
int Point::GetX(void)
{
	return xpos;
}
int Point::GetY(void)
{
	return ypos;
}

void Circle::Init(Point pcen, int radius)
{
	xCen = pcen.GetX();
	yCen = pcen.GetY();
	rad = radius;
}
int Circle::GetXCen(void)
{
	return xCen;
}
int Circle::GetYCen(void)
{
	return yCen;
}
int Circle::GetRadius(void)
{
	return rad;
}

void Ring::Init(Circle ic, Circle oc)
{
	inXCen = ic.GetXCen();
	inYCen = ic.GetYCen();
	outXCen = oc.GetXCen();
	outYCen = oc.GetYCen();
	inRad = ic.GetRadius();
	outRad = oc.GetRadius();
}
void Ring::ShowRingInfo(void) const
{
	cout << "Inner Circle Info" << endl;
	cout << "radius: " << inRad << endl;
	cout << "[" << inXCen << ", " << inYCen << "]" << endl;

	cout << "Outter Circle Info" << endl;
	cout << "radius: " << outRad << endl;
	cout << "[" << outXCen << ", " << outYCen << "]" << endl;
}