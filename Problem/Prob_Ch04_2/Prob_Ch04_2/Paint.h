#pragma once

class Point
{
private:
	int xpos, ypos;

public:
	void Init(int x, int y);
	void ShowPointInfo() const;
	int GetX(void);
	int GetY(void);
};

class Circle
{
private:
	int xCen;
	int yCen;
	int rad;

public:
	void Init(Point pcen, int radius) ;
	int GetXCen(void);
	int GetYCen(void);
	int GetRadius(void);
};

class Ring
{
private:
	int inXCen, inYCen, outXCen, outYCen;
	int inRad, outRad;

public:
	void Init(Circle ic, Circle oc);
	void ShowRingInfo(void) const;
};