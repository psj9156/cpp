#include "Paint.h"

int main(void)
{
	Point innerCen;
	Point outterCen;

	innerCen.Init(1, 1);
	outterCen.Init(2, 2);

	Circle inner;
	Circle outter;

	inner.Init(innerCen, 4);
	outter.Init(outterCen, 9);

	Ring ring;

	ring.Init(inner, outter);
	ring.ShowRingInfo();

	return 0;
}